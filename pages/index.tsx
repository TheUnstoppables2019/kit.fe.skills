import type { NextPage } from "next";
import Head from "next/head";
import Image from "next/image";
import { homeColorTheme } from "../config/apis";
import ImgContainer from "../components/ImgContainer/ImgContainer";
import styles from "../styles/Home.module.scss";
import Menu from "../components/Menu/Menu";
import Category from "../components/Category/Category";
import { getSkills } from "@/api/kitmanyiuapis";

export async function getServerSideProps() {
  const res: any = await getSkills();
  return {
    props: {
      skillsData: res.data,
    },
  };
}

const Home: NextPage = (props: any) => {
  const { skillsData } = props;
  let i = 1;
  return (
    <div>
      <Head>
        <title>Kitman Yiu | Skills ©</title>
        <meta
          name="description"
          content="React, Node, JAVA and more. A details look of Kitman skills"
        />
        <meta
          property="og:title"
          content="Kitman Yiu | Skills | Home"
          key="title"
        />
        <meta
          property="og:description"
          content="React, Node, JAVA and more. A details look of Kitman skills"
        />
        <meta
          name="twitter:title"
          content="React, Node, JAVA and more. A details look of Kitman skills"
        />
        <meta
          name="twitter:description"
          content="React, Node, JAVA and more. A details look of Kitman skills"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <Menu />

        <section className={styles.bg} style={{ marginBottom: "150px" }}>
          <div className="shape shape-style-1 shape-primary">
            <span className="circle-150" />
            <span className="circle-50 delay-2s" />
            <span className="circle-50" />
            <span className="circle-75" />
            <span className="circle-100 delay-2s" />
            <span className="circle-75" />
            <span className="circle-50 delay-5s" />
            <span className="circle-100 delay-1s" />
            <span className="circle-50 delay-1s" />
            <span className="circle-100 delay-5s" />
          </div>
          <div className={styles.header__container}>
            <div>
              <h1 className={styles.header__title}>
                React | Node | JAVA | AWS
              </h1>
            </div>
          </div>

          <div className={styles.shape__bottom}>
            <div className={styles.deco__bottom}></div>
          </div>
        </section>
        {Object.keys(skillsData).map((key: any) => {
          i *= -1;
          const color: string = skillsData[key].themeColor
            ? skillsData[key].themeColor
            : "";
          const bgColor = homeColorTheme[color]?.bgColor;
          const title = `${key} SKILLS`;
          console.log(title);
          const spiltTitle = title.split(" ");
          if (i === 1) {
            return (
              <div key={key}>
                <div className={styles.section__header}>
                  <div className={styles.deco__top}></div>
                  <div className={styles.full__width}>
                    <div className={"inline-block"}>
                      <h2 className="global_h2">
                        {spiltTitle[0]}
                        <span className="global_color--grey">
                          {" "}
                          {spiltTitle[1]}
                        </span>
                        <span className="global_background">
                          {spiltTitle[0]}
                        </span>
                        {!key.toUpperCase().includes("FRONTEND")
                          ? " (IN DEVELOP)"
                          : ""}
                      </h2>
                    </div>
                  </div>
                  <div className={styles.section__fe}>
                    {Object.keys(skillsData[key]).map((item: any) => {
                      return (
                        <Category
                          item={skillsData[key][item]}
                          category={key}
                          stack={item}
                          key={skillsData[key][item].name}
                        />
                      );
                    })}
                  </div>
                  <div className={styles.deco__bottom}></div>
                </div>
              </div>
            );
          }
          return (
            <>
              <div className={[styles.full__width, "text-center"].join(" ")}>
                <div className={"inline-block"}>
                  <h2 className="global_h2">
                    {spiltTitle[0]}
                    <span className="global_color--grey"> {spiltTitle[1]}</span>
                    <span className="global_background">{spiltTitle[0]}</span>
                    {!key.toUpperCase().includes("FRONTEND")
                      ? " (IN DEVELOP)"
                      : ""}
                  </h2>
                </div>
              </div>
              <div className={styles.section__fe}>
                {Object.keys(skillsData[key]).map((item: any) => {
                  return (
                    <Category
                      item={skillsData[key][item]}
                      category={key}
                      stack={item}
                      key={skillsData[key][item].name}
                    />
                  );
                })}
              </div>
            </>
          );
        })}
      </main>
      <footer className={styles.footer}>
        <div className={styles.deco__top}></div>
        <p>© Copyright 0101001 By Kitman Yiu</p>
      </footer>
    </div>
  );
};

export default Home;
